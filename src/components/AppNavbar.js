import { Link } from 'react-router-dom'
import { useContext } from 'react';

import { Nav,Navbar,Container,NavDropdown } from 'react-bootstrap';
import './AppNavbar.css';

import { list } from 'cart-localstorage';
import UserContext from '../userContext';

export default function AppNavbar(){

	const {user} = useContext(UserContext)

	const cart = list()

	const cartLength = cart.length


	return (
		
		<Navbar collapseOnSelect expand="lg" bg="black" variant="dark" className="shadow mb-5">
			<Container>
				<Navbar.Brand>
		  			<Link to="/"><img src="https://www.creativefabrica.com/wp-content/uploads/2019/03/Monogram-OF-Logo-Design-by-Greenlines-Studios.jpg" id="logo" alt="pekchur"/></Link>
		  		</Navbar.Brand>
		 		<Navbar.Toggle aria-controls="responsive-navbar-nav" />
		    	<Navbar.Collapse>
		    		<Nav className="me-auto">
		      			<Link to="/" className="nav-link">Home</Link>
		      			<NavDropdown title="Products">
		      				<a href="/#hot" className="nav-link text-dark px-3">Hot🔥</a>
		      				<a href="/#new" className="nav-link text-dark px-3">Latest</a>
		        			<NavDropdown.Divider />
		        			<Link to="/products" className="nav-link text-dark px-3">All</Link>
		      			</NavDropdown>
		    			{
		      				user.id
		      				?<>
		      					<Link to="/orders" className="nav-link">Orders</Link>
		      						{
		      							user.isAdmin
		      							?<></>
		      							:<Link to="/cart" className="nav-link">🛒{cartLength}</Link>
		      						}
		      				 </>
		      				:<></>
		    			}
		    		</Nav>
		    		<Nav>
		    			{
		    				user.isAdmin
		    				?<Nav.Link href="/addProduct" className='text-white text-right'>Add Product</Nav.Link>
		    				:<></>
		    			}
		    			{
		    				user.id
		    				?<Nav.Link href="/logout" className='text-white text-right'>Logout</Nav.Link>
		    				:<Nav.Link href="/login" className='text-white text-right'>Login</Nav.Link>
		    			}
		    		</Nav>
		  		</Navbar.Collapse>		  
		  </Container>
		</Navbar>
	)
}
