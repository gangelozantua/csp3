import { useContext } from 'react';
import { Link } from 'react-router-dom';
import UserContext from '../userContext';

import { Card } from 'react-bootstrap';

import './Product.css'

export default function ProductCard({productProp}){

	const{user} = useContext(UserContext);


	return(
		
		<Card className="bg-dark shadow my-2 mx-auto col-md-3">
			<Card.Img src={productProp.imageUrl} id="cardImg" />
			{
				user.isAdmin
				?	
					<>
					<Link to ={`/products/updateProduct/${productProp._id}`}>
					<Card.ImgOverlay>
					 	<Card.Title className="text-black">P{productProp.price}</Card.Title>
					</Card.ImgOverlay>
				 	</Link>
				 	</>
					
				:
					<>
					<Link to ={`/products/viewProduct/${productProp._id}`}>
					<Card.ImgOverlay>
					  	<Card.Title className="text-black"><span className="bg-white">P{productProp.price}</span></Card.Title>
					</Card.ImgOverlay>
				 	</Link>
				 	</>
					
			}	  
			<Card.Body className="bg-dark text-white">
		    	<Card.Title>{productProp.name}</Card.Title>
		    	<Card.Text>
		        	{productProp.description}
		    	</Card.Text>
		    </Card.Body>
		</Card>

	)
}
