import { useState,useContext } from 'react';
import { Navigate } from 'react-router-dom';

import { Form,Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

import UserContext from '../userContext';


export default function AddProduct(){

	const {user} = useContext(UserContext)

	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)
	const [imageUrl, setImageUrl] = useState("")
	
	//create a product
	function createProduct(e){

		e.preventDefault()

		fetch('https://sheltered-crag-61892.herokuapp.com/products/',{

			method: 'POST',
			headers: {

				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({

				name: name,
				description: description,
				price: price,
				imageUrl: imageUrl

			})
		})
		.then(res => res.json())
		.then(data => {

			if(data._id){
				Swal.fire({

					icon:"success",
					title: "Product Creation Successful"

				})
				window.location = "./products"
			} else {
				Swal.fire({

					icon: "error",
					title: "Product Creation Failed.",
					text: data.message

				})
			}	
		})
	}
	

	return(

		user.isAdmin=false
		?<Navigate to="/" replace={true} />
		:
		<>
			<h1 className='my-5 text-center'>Add a Product</h1>
			<Form onSubmit={e => createProduct(e)}>
				<Form.Group>
					<Form.Label>Name:</Form.Label>
					<Form.Control type="text" placeholder="Enter Product Name" required value={name} onChange={e => {setName(e.target.value)}} />
				</Form.Group>

				<Form.Group>
					<Form.Label>Description:</Form.Label>
					<Form.Control type="text" placeholder="Enter Description" required value={description} onChange={e => {setDescription(e.target.value)}} />
				</Form.Group>

				<Form.Group>
					<Form.Label>Price:</Form.Label>
					<Form.Control type="number" required value={price} onChange={e => {setPrice(e.target.value)}} />
				</Form.Group>

				<Form.Group>
					<Form.Label>Image Url:</Form.Label>
					<Form.Control type="text" value={imageUrl} onChange={e => {setImageUrl(e.target.value)}} />
				</Form.Group>
							
				<Button variant="outline-info" type="submit" className="my-5">Submit</Button>
			</Form>
		</>
	)
}