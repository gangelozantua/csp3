import { useState,useContext,useEffect } from 'react';
import { Row,Form,Button,Container } from 'react-bootstrap';

//components
import ProductCard from '../components/Product';
import AdminDashboard from '../components/AdminDashboard';

import UserContext from '../userContext';

export default function Products(){

	const {user} = useContext(UserContext)

	const [productArray,setProductArray] = useState([])

	const [name, setName] = useState("")

	useEffect(()=>{

		fetch("https://sheltered-crag-61892.herokuapp.com/products/active")
		.then(res => res.json())
		.then(data => {

			setProductArray(data.map(product =>{

				return (

					<ProductCard key={product._id} productProp={product} />
				)
			}))
		})
	},[])

	//product search
	function findProduct(e){

		e.preventDefault()

		fetch('https://sheltered-crag-61892.herokuapp.com/products/findProductName',{

			method: 'POST',
			headers: {

				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({

				name: name

			})
		})
		.then(res => res.json())
		.then(data => {

			setProductArray(data.map(product =>{

				return (

					<ProductCard key={product._id} productProp={product} />
				)
			}))
		
		})
	}


	return(
		user.isAdmin
		? <AdminDashboard />
		:
		<>
			<Container>
				<Form onSubmit={e => findProduct(e)}>
					<Row className= "justify-content-center my-5">
						<Form.Group className="col-8 col-md-6">
							<Form.Control type="text" placeholder="Search" value={name} onChange={e => {setName(e.target.value)}} />
						</Form.Group>
						<Button type="submit" className="col-3 col-md-2 btn-primary">Submit</Button>
					</Row>
				</Form>				
					<Row>
					{productArray}
					</Row>
			</Container>		
		</>
	)
}