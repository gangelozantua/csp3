import { useState,useEffect } from 'react';
import { useParams } from 'react-router-dom';

import { add } from 'cart-localstorage' ;

import { Form,Card,Button,Row } from 'react-bootstrap';



export default function ViewProduct(){

	const {productId} = useParams()

	const [quantity,setQuantity] = useState(1);

	const [productDetails,setProductDetails] = useState({

		name: null,
		description: null,
		price: null,
		imageUrl: null,
	})


	useEffect(()=>{

		fetch(`https://sheltered-crag-61892.herokuapp.com/products/getSingleProduct/${productId}`)
		.then(res => res.json())
		.then(data => {
			
			setProductDetails({

				name: data.name,
				description: data.description,
				price: data.price,
				imageUrl: data.imageUrl,
			})
		})
	},[productId])


	//add a product to cart
	function addToCart(e){
		e.preventDefault()

		let cartItem = {

			id: productId,
			productId: productId,
			name: productDetails.name,
			price: productDetails.price,
			imageUrl: productDetails.imageUrl
			
		}

		add(cartItem, parseInt(quantity))
		window.location = "../"

	}


	return(
		<>
			<Row className="mt-5">
				<img src={productDetails.imageUrl} className="col-12 col-md-6 mt-5 border-black" alt={productDetails.name} />
				<Card className="mt-5 col-12 col-md-6">
					<Card.Body>
						<Card.Title className="mb-4">{productDetails.name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{productDetails.description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>{productDetails.price}</Card.Text>
					</Card.Body>

					<Form onSubmit={e => addToCart(e)}>
						<Form.Group className="col-2 col-md-2 mx-3">
							<Form.Label>Quantity:</Form.Label>
							<Form.Control type="number" className="col-2 col-md-2" value={quantity} onChange={e => {setQuantity(e.target.value)}} />					
						</Form.Group>

						<h4 className="mx-3">Total:{productDetails.price*quantity}</h4>					
						<Button variant="outline-info" type="submit" className="my-5 mx-3">Add to 🛒</Button>					
					</Form>	
				</Card>
			</Row>
		</>
	)
}

