import { useContext,useEffect } from 'react';
import UserContext from '../userContext';
import { Navigate } from 'react-router-dom';
import { destroy } from 'cart-localstorage';

export default function Logout(){

	const {setUser,unsetUser} = useContext(UserContext)

	unsetUser();

	useEffect(()=>{

		setUser({

			id: null,
			isAdmin: null
		})
		destroy()

	},[])


	return (

		<Navigate to="/login" replace ={true} />
	)
}