import { useState,useContext,useEffect } from 'react';
import { Navigate,Link } from 'react-router-dom';

import { Form,Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

import UserContext from '../userContext';



export default function Login(){

	const {user,setUser} = useContext(UserContext)

	const [email,setEmail] = useState("");
	const [password,setPassword] = useState("");

	const [isActive,setIsActive] = useState(false);

	useEffect(()=>{

		if(email !== "" && password !== ""){
			setIsActive(true)
		}
		else {
			setIsActive(false)
		}

	},[email,password])

	//user login
	function loginUser(e){

		e.preventDefault()

		fetch('https://sheltered-crag-61892.herokuapp.com/users/login',{

			method: 'POST',
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
				})
			
			})
			.then(res => res.json())
			.then(data => {

				if(data.accessToken){
					Swal.fire({

						icon:"success",
						title:"Login Succesfull.",
						text: "Welcome!"
					})

					localStorage.setItem('token',data.accessToken)


					let token = localStorage.getItem('token')

					fetch('https://sheltered-crag-61892.herokuapp.com/users/',{

						method: 'GET',
						headers: {
							'Authorization': `Bearer ${token}`
						}
					})
					.then(res => res.json())
					.then(data => {

						setUser({
							id: data._id,
							isAdmin: data.isAdmin
						})
					})
				}
				else{
					Swal.fire({

						icon:"error",
						title:"Login Failed",
						text:data.message
					})
				}
			})
	}
	

	return(
		user.id
		?
		<Navigate to="/" replace={true} />
		:
		<>
			<h1 className="my-5">Login</h1>
			<Form onSubmit={e => loginUser(e)} className="col-12 col-md-6">
				<Form.Group>
					<Form.Label>Email:</Form.Label>
					<Form.Control type="text" placeholder="Enter Email" required value={email} onChange={e => {setEmail(e.target.value)}} />
				</Form.Group>
				<Form.Group>
					<Form.Label>Password:</Form.Label>
					<Form.Control type="password" required value={password} onChange={e => {setPassword(e.target.value)}} />
				</Form.Group>
				{
					isActive
					? <Button variant="primary" type="submit" className="my-5">Submit</Button>
					:<Button variant="primary" disabled className="my-5">Submit</Button>
				}
				
			</Form>
			<Link to="../register">Don't have an account? Register Here.</Link>
		</>
	)
}
