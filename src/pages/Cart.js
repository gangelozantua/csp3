import { useState, useEffect } from 'react';
import { list,destroy,remove } from 'cart-localstorage'
import { Table,Button } from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function Cart(){

	const cart = list()

	const [allCartItems, setAllCartItems] = useState([])

	useEffect(()=>{
		
		setAllCartItems(cart.map(item =>{
			
			return(
				<tr key={item.id}>
					<td>{item.name}</td>
					<td>{item.price}</td>
					<td>{item.quantity}</td>						
					<td><Button variant="danger" className="mx-2" onClick={id => removeItem(item.id)}>Remove</Button></td>						
				</tr>

			)

		}))
	},[])

	//purchase cart
	function purchase(){

		fetch('https://sheltered-crag-61892.herokuapp.com/orders/',{
			method: 'POST',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				products: cart
			})
		})
		.then(res => res.json())
		.then(async(data) => {

			if(data.message){
				Swal.fire({
					icon: "success",
					title: "Purchased Successfully.",
					text: data.message
				})
				.then(() =>{
				    window.location = "/";

				});
			}
			else {
				Swal.fire({
					icon: "error",
					title: "Purchase Failed",
					text: data.message
				})
			}
			await destroy()
		})

		
	}

	//remove cart item
	function removeItem(id){

		remove(id)
		window.location = "/cart"
	}
	
	//total price of cart
	let totalAmount = 0
	function total(){

		cart.map(item =>{
								
			totalAmount += item.price * item.quantity
			
			return totalAmount
				
		})
	}
	total()


	return(
		
		<>
			<h1 className="my-5 text-center">Cart</h1>
			<Table striped bordered hover>
				<thead>
					<tr>
						<th>Name</th>
						<th>Price</th>
						<th>Quantity</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
						{allCartItems}
				</tbody>		
			</Table>
			
			<h5>Total: P{totalAmount}</h5>
			<Button variant="primary" type="submit" className="my-5 mx-2" onClick={purchase}>Purchase</Button>
			<a href="/products" className="btn btn-primary">Go back to Products</a>		
		</>	
	)
}
