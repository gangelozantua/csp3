import { useEffect,useState } from 'react';
import { Row,Container } from 'react-bootstrap';

import Banner from '../components/Banner';
import ProductCard from '../components/Product';

export default function Home(){

	let bannerData = {
		title: "onlyFans",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Euismod in pellentesque massa placerat duis ultricies. Pharetra sit amet aliquam id diam maecenas ultricies. Malesuada fames ac turpis egestas. Non diam phasellus vestibulum lorem sed. Tempus egestas sed sed risus pretium. Semper viverra nam libero justo laoreet sit amet. Ullamcorper malesuada proin libero nunc consequat interdum varius. Scelerisque mauris pellentesque pulvinar pellentesque habitant. Nec tincidunt praesent semper feugiat nibh sed pulvinar proin.",
		destination: "/products",
		buttonText: "View Catalog"
	}

	const [productArray1,setProductArray1] = useState([])
	const [productArray2,setProductArray2] = useState([])

	useEffect(()=>{

		fetch("https://sheltered-crag-61892.herokuapp.com/products/hot")
		.then(res => res.json())
		.then(data => {

			setProductArray1(data.map(product =>{

				return (

					<ProductCard key={product._id} productProp={product} />
				)
			}))
		})
	},[])
	
	useEffect(()=>{

		fetch("https://sheltered-crag-61892.herokuapp.com/products/new")
		.then(res => res.json())
		.then(data => {

			setProductArray2(data.map(product =>{

				return (

					<ProductCard key={product._id} productProp={product} />
				)
			}))
		})
	},[])

	
	return (
		<>	
			<Banner bannerProp={bannerData} />
			<Container fluid>
			<h1 id="hot">Hot</h1>
			
			<Row className="d-flex justify-content-between">
			
			{productArray1}
			
			</Row>
			
			<h1 id="new">New Arrivals</h1>
			<Row>
			{productArray2}
			
			</Row>
			</Container>
			
		</>
	)
}