import { useState,useEffect } from 'react';
import { useParams } from 'react-router-dom';

import { Form,Button } from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function UpdateProduct(){

	const {productId} = useParams()

	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)
	const [imageUrl, setImageUrl] = useState("")

	const [productDetails,setProductDetails] = useState({

		name: null,
		description: null,
		price: null,
		imageUrl: null,
	})

	useEffect(()=>{

		fetch(`https://sheltered-crag-61892.herokuapp.com/products/getSingleProduct/${productId}`)
		.then(res => res.json())
		.then(data => {
		
			setProductDetails({

				name: data.name,
				description: data.description,
				price: data.price,
				imageUrl: data.imageUrl,
			})
		})
	},[productId])

	//change product details
	function updateProduct(e){

		e.preventDefault()

		fetch(`https://sheltered-crag-61892.herokuapp.com/products/${productId}`,{

			method: 'PUT',
			headers: {
				
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({

				name: name,
				description: description,
				price: price,
				imageUrl: imageUrl

			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({

					icon:"success",
					title: "Product Update Successful"

				})
				window.location = "../"
			} else {
				Swal.fire({

					icon: "error",
					title: "Product Update Failed.",
					text: data.message

				})
			}
		
		})
	}


	return (

		<>
			<h1 className='my-5 text-center'>Update Product</h1>
			<Form onSubmit={e => updateProduct(e)}>
				<Form.Group>
					<Form.Label>Name:</Form.Label>
					<Form.Control type="text" required value={name} placeholder={`${productDetails.name}`} onChange={e => {setName(e.target.value)}} />
				</Form.Group>
				<Form.Group>
					<Form.Label>Description:</Form.Label>
					<Form.Control type="text" required value={description} placeholder={`${productDetails.description}`} onChange={e => {setDescription(e.target.value)}} />
				</Form.Group>
				<Form.Group>
					<Form.Label>Price:</Form.Label>
					<Form.Control type="number" required value={price} placeholder={`${productDetails.price}`} onChange={e => {setPrice(e.target.value)}} />
				</Form.Group>
				<Form.Group>
					<Form.Label>Image Url:</Form.Label>
					<Form.Control type="text" required value={imageUrl} placeholder={`${productDetails.imageUrl}`} onChange={e => {setImageUrl(e.target.value)}} />
				</Form.Group>
			
				<Button variant="outline-info" type="submit" className="my-5">Submit</Button>

			</Form>
		</>
	)
}
