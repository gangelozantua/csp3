import { BrowserRouter as Router } from 'react-router-dom';
import { Route,Routes } from 'react-router-dom';

import { useState,useEffect } from 'react';
import { Container } from 'react-bootstrap';
import './App.css';

import { UserProvider } from './userContext';

import AppNavbar from './components/AppNavbar';

//pages
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import AddProduct from './pages/AddProduct';
import Products from './pages/Products';
import ViewProduct from './pages/ViewProduct';
import UpdateProduct from './pages/UpdateProduct';
import Orders from './pages/Orders';
import Cart from './pages/Cart';
import Error from './pages/Error';

export default function App(){

  const [user,setUser] = useState({

    id: null,
    isAdmin: null
  })

  useEffect(()=>{

    fetch('https://sheltered-crag-61892.herokuapp.com/users/',{

      method: 'GET',
      headers: {

        'Authorization' : `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {

      setUser({

        id: data._id,
        isAdmin: data.isAdmin
      })

    })
  },[])

  const unsetUser = () => {

    localStorage.clear()
  }

  return (
    
    <>
      <UserProvider value={{user,setUser,unsetUser}}>
        <Router>
          <AppNavbar />
          <Container>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/login" element={<Login />} />
              <Route path="/logout" element={<Logout />} />
              <Route path="/register" element={<Register />} />
              <Route path="/addProduct" element={<AddProduct />} />
              <Route path="/products" element={<Products />} />             
              <Route path="/products/viewProduct/:productId" element={<ViewProduct />} />
              <Route path="/products/updateProduct/:productId" element={<UpdateProduct />} />
              <Route path="/orders" element={<Orders />} />
              <Route path="/cart" element={<Cart />} />
              <Route path="/error" element={<Error />} />
            </Routes>
          </Container>
        </Router>

      </UserProvider>       
    </>
  )
}


